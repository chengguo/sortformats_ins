# |/usr/bin/env python3

"""Sort a list of formats according to their level of compression.
Use the insertion algorithm"""

import sys

# Ordered list of image formats, from lower to higher insertion
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    """Find out if format1 is lower than format2
    Returns True if format1 is lower, False otherwise.
    A format is lower than other if it is earlier in the fordered list.
    """
    format1 = fordered.index(format1)
    format2 = fordered.index(format2)
    if format1 < format2:
        return True
    elif format1 > format2:
        return False



def sort_pivot(formats: list, pivot: int):
    """Sort the pivot format, by exchanging it with the format
    on its left, until it gets ordered.
    """
    lower = pivot
    for pos in range(pivot + 1, len(formats)):
        if lower_than(formats[pos], formats[lower]):
            lower = pos

    return lower


def sort_formats(formats: list) -> list:
    """Sort formats list
    Returns the sorted list of formats, according to their
    position in fordered. Use the insertion algorithm"""
    for pivot in range(len(formats)):
        lower = sort_pivot(formats, pivot)
        formats[pivot], formats[lower] = formats[lower], formats[pivot]

    return formats


def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""

    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()


if __name__ == '__main__':
    main()
